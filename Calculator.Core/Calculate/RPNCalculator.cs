﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calculator.Core
{
    /// <summary>
    /// Класс вычисления выражения методом Обратной Польской Записи
    /// </summary>
    public class RPNCalculator : ICalculate
    {
        /// <summary>
        /// Получение обратной польской записи
        /// </summary>
        /// <param name="inputString">выражение</param>
        /// <returns>строку ОПЗ</returns>
        public string GetIntermediaryString(string inputString)
        {
            StringBuilder rpnBuilder = new StringBuilder();
            Stack<Char> stack = new Stack<Char>();
            char temp;
            int priority;
            int currentPriority;

            if (String.IsNullOrEmpty(inputString))
                return inputString;

            for (int i = 0; i < inputString.Length; )
            {
                char item = inputString[i];
                if (Char.IsLetterOrDigit(item))
                {
                    while (Char.IsLetterOrDigit(item) || item == '.')
                    {
                        rpnBuilder.Append(item);
                        i++;
                        if (i == inputString.Length)
                            break;

                        item = inputString[i];
                    }
                    rpnBuilder.Append(" ");
                }
                else if (item == '+' || item == '-'
                      || item == '*' || item == '/'
                      || item == '^' || item == '('
                      || item == ')')
                {
                    i++;
                    priority = GetPriority(item);
                    if (stack.Count == 0)
                    {
                        stack.Push(item);
                    }
                    else if (item == '(')
                    {
                        stack.Push(item);
                    }
                    else if (item == ')')
                    {
                        while (stack.Count != 0)
                        {
                            if (stack.Peek() == '(')
                                break;
                            if (stack.Count != 0 && stack.Peek() != '(')
                            {
                                rpnBuilder.Append(stack.Pop());
                                rpnBuilder.Append(" ");
                            }

                        }
                        if (stack.Count != 0 && stack.Peek() == '(')
                        {
                            stack.Pop();
                        }
                    }
                    else if (stack.Count != 0)
                    {
                        temp = stack.Peek();
                        currentPriority = GetPriority(temp);

                        if (currentPriority < priority)
                        {
                            stack.Push(item);
                        }

                        else if (currentPriority >= priority)
                        {
                            while (stack.Count != 0)
                            {
                                temp = stack.Peek();
                                currentPriority = GetPriority(temp);

                                if (currentPriority >= priority)
                                {
                                    rpnBuilder.Append(stack.Pop());
                                    rpnBuilder.Append(" ");
                                }
                                else
                                    break;

                            }
                            stack.Push(item);
                        }

                    }
                }
                
            }
            while (stack.Count != 0)
            {
                if (stack.Peek() == '(')
                    stack.Pop();
                else
                {
                    rpnBuilder.Append(stack.Pop());
                    rpnBuilder.Append(" ");
                }
            }

            return rpnBuilder.ToString();

        }


        /// <summary>
        /// Вычисление по строке польской обратной записи выражения
        /// </summary>
        /// <param name="rpnString">срока польской обратной записи</param>
        /// <returns>число</returns>
        public double CalculateIntermediaryString(string rpnString)
        {
            double first;
            double second;
            Stack<IComparable> newstack = new Stack<IComparable>();
            // для перевода в Double
            rpnString = rpnString.Replace(".", ",");
            string[] array = rpnString.Split(new char[] { ' ' });

            for (int i = 0; i < array.Length; i++)
            {
                var item = array[i];
                double number;
                bool isparse = Double.TryParse(item, out number);
                if (isparse)
                {
                    newstack.Push(item);
                }

                if (item.Equals("+") || item.Equals("-")
                        || item.Equals("*") || item.Equals("/")
                        || item.Equals("^"))
                {
                    String firstSymbols = "";
                    String secondSymbols = "";
                    firstSymbols += newstack.Pop();
                    secondSymbols += newstack.Pop();
                    Double.TryParse(firstSymbols, out first);
                     Double.TryParse(secondSymbols, out second);

                    switch (array[i])
                    {
                        case "/":
                            newstack.Push(second / first);
                            break;
                        case "*":
                            newstack.Push(second * first);
                            break;
                        case "+":
                            newstack.Push(second + first);
                            break;
                        case "-":
                            newstack.Push(second - first);
                            break;

                    }
                }

            }
            var temp = newstack.Peek();
            return Convert.ToDouble(temp);
        }

  
        /// <summary>
        /// Валидация вычисляемого выражения
        /// </summary>
        /// <param name="line">строка выражения</param>
        /// <returns></returns>
        public bool IsValidExpression(string line)
        {
            Regex regex = new Regex("(^[A-Za-z0-9(][A-Za-z0-9-+/*.()]+[A-Za-z0-9]$)|(^[A-Za-z0-9]$)", RegexOptions.IgnoreCase);
            return regex.IsMatch(line);
        }

        /// <summary>
        /// Преобразует входную строку в переменную и вычисляемое выражение 
        /// </summary>
        /// <param name="line">входная строка</param>
        /// <param name="variable">значение переменной</param>
        /// <param name="expression">значение выражения</param>
        /// <returns>Успешно ли выполнена операция</returns>
        public bool TryGetPartsOfExpression(string line, out string variable, out string expression)
        {
            variable = String.Empty;
            expression = String.Empty;

            Regex regex = new Regex("^[A-Za-z][A-Za-z]*=[A-Za-z0-9-+/*.()]+", RegexOptions.IgnoreCase);
            if (!regex.IsMatch(line))
                return false;



            int indexEquality = line.IndexOf("=");
            if (line[indexEquality + 1] == '-')
            {
                int indexOperation = line.IndexOfAny(new char[] { '+', '-', '*', '/' }, indexEquality + 2);
                if(indexOperation > 0)
                {
                    char operation = line[indexOperation];
                    switch (operation)
                    {
                        case '+':
                        case '-':
                            line = line.Substring(0, indexEquality+1) + "0" + line.Substring(indexEquality + 1);
                            break;
                        case '*':
                        case '/':
                            line = line.Substring(0, indexEquality + 1) + "(0" 
                                   + line.Substring(indexEquality + 1, indexOperation-2) + ")"
                                   + line.Substring(indexOperation);
                            break;
                    }
                }
                
            }
            variable = line.Substring(0, indexEquality);
            expression = line.Substring(indexEquality + 1, line.Length - (indexEquality + 1));
            return true;
        }


        /// <summary>
        /// Получение приоритета операции
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        private static int GetPriority(char symbol)
        {
            int result = 0;
            switch (symbol)
            {
                case '^':
                    result = 3;
                    break;
                case '/':
                case '*':
                    result = 2;
                    break;
                case '+':
                case '-':
                    result = 1;
                    break;
                case '(':
                    result = 0;
                    break;
            }
            return result;
        }
    }
}
