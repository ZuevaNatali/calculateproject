﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator.Core
{
    /// <summary>
    /// контракт на вычисление выражений
    /// </summary>
    public interface ICalculate : IValidateExpression
    {
        /// <summary>
        /// получение промежуточной строки 
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        string GetIntermediaryString(string inputString);

        /// <summary>
        /// Вычисление промежуточной строки
        /// </summary>
        /// <param name="rpnString"></param>
        /// <returns></returns>
        double CalculateIntermediaryString(string rpnString);

    }
}
