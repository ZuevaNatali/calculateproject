﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator.Core
{
    /// <summary>
    /// контракт валидации выражений
    /// </summary>
    public interface IValidateExpression
    {
        /// <summary>
        /// Валидация вычисляемого выражения
        /// </summary>
        /// <param name="line">строка выражения</param>
        /// <returns></returns>
        bool IsValidExpression(string line);

        /// <summary>
        /// Преобразует входную строку в переменную и вычисляемое выражение 
        /// </summary>
        /// <param name="line">входная строка</param>
        /// <param name="variable">значение переменной</param>
        /// <param name="expression">значение выражения</param>
        /// <returns></returns>
        bool TryGetPartsOfExpression(string line, out string variable, out string expression);
    }
}
