﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator.Core
{
    public class Constants
    {
        public const string UnknownError = "Операция не выполнена. Произошла неизвестная ошибка.";
        public const string ExpressionError = "Операция не выполнена. Ошибка валидирования выражения.";
        public const string ExistError = "Операция не выполнена. Указанная переменная уже существует в системе.";
        public const string NotValueVariable = "Значение переменной невозможно получить";
    }
}
