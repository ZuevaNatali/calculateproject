﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Core.DAL
{
    /// <summary>
    /// Класс инициализации  модели данных
    /// </summary>
    public static class ContextInitializer
    {
        /// <summary>
        /// Проверка модели данных
        /// </summary>
        public static void CheckModel()
        {
            using (MappingContext context =  new MappingContext())
            {
                bool result = context.Database.CreateIfNotExists();
                DoMigration();
            }
        }

        /// <summary>
        /// Обновление модели данных, если имеются невыполненные миграции в сборке
        /// </summary>
        private static void DoMigration()
        {
            var migrator = new DbMigrator(new ContextConfiguration());
            migrator.Update();
        }


    }
}
