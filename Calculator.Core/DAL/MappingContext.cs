﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Core.DAL
{
    public class MappingContext : DbContext
    {
        public DbSet<Formula> Formuls { get; set; }

        #region Constructors

        public MappingContext()
        {
            Database.SetInitializer<MappingContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public MappingContext(string connString)
            : base(connString)
        {
        }

        public MappingContext(System.Data.Common.DbConnection connection)
            : base(connection, true)
        {
        }

        #endregion



    }
}
