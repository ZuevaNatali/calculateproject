﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Core.DAL
{
    /// <summary>
    /// Класс настройки конфигурации миграций для модели данных MappingContext
    /// </summary>
    public class ContextConfiguration : DbMigrationsConfiguration<MappingContext>
    {
        public ContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }
    }
}
