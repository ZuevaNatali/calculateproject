﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Core.DAL
{
    /// <summary>
    /// Сущность хранения данных о формулах
    /// </summary>
    [Table("Formuls")]
    public class Formula
    {
        /// <summary>
        /// Id записи
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Название переменной
        /// </summary>
        [MaxLength(10)]
        public string VariableName { get; set; }

        /// <summary>
        /// Начальная формула
        /// </summary>
        [MaxLength(400)]
        public string InitFormula { get; set; }

        /// <summary>
        /// Промежуточный вид формулы
        /// </summary>
        [MaxLength(400)]
        public string IntermediateValue { get; set; }

        /// <summary>
        /// Результат
        /// </summary>
        public double? Result { get; set; }

        public Formula()
        {
            Id = Guid.NewGuid();
        }

    }
}
