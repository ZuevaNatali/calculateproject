namespace Calculator.Core.DAL
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Formuls",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        VariableName = c.String(maxLength: 10),
                        InitFormula = c.String(maxLength: 400),
                        IntermediateValue = c.String(maxLength: 400),
                        Result = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Formuls");
        }
    }
}
