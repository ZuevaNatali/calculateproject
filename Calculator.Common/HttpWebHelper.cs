﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace Calculator.Common
{
    public static class HttpWebHelper
    {
        public static T GetContent<T>(string url)           
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse responseWeb = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(responseWeb.GetResponseStream());

                var serializer = new XmlSerializer(typeof(T));
                var response = (T)serializer.Deserialize(reader);

                return response;
            }
            catch(Exception ex)
            {
                return default(T);
            }
        }
    }
}
