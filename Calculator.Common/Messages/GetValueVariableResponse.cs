﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Common.Messages
{
    [Serializable]
    [DataContract(Namespace="")]
    public class GetValueVariableResponse : BaseResponse
    {
        [DataMember(Order = 3)]
        public double Value { get; set; }
    }
}
