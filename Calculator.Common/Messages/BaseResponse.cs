﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Common.Messages
{
    [Serializable]
    [DataContract(Namespace="")]
    public abstract class BaseResponse
    {
        [DataMember(Order = 1)]
        public StatusType StatusCode { get; set; }

        [DataMember(Order = 2)]
        public string ErrorMessage { get; set; }
    }
}
