﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Common.Messages
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class CreateVariableResponse : BaseResponse
    {
    }
}
