﻿using Calculator.Core.DAL;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Routing;

namespace CalculateService.App_Start
{
    public static class Startup
    {
        public static object lockCalc = new object();
        public static readonly ILog _log = LogManager.GetLogger(typeof(Startup));

        public static void Init()
        {
            _log.InfoFormat("Initialization...");
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ContextInitializer.CheckModel();
        }


        /// <summary>
        /// Подсчет еще невысчетанных выражений в фоновом режиме
        /// </summary>
        public static void MonitorCalcExpression()
        {
            _log.InfoFormat("MonitorCalcExpression start");
            while (true)
            {
                Thread.Sleep(10000);

                lock (lockCalc)
                {
                    CalculateHelper.CalculateExpressions();
                }
            }
        }
    }
}