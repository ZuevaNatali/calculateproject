﻿using Autofac.Integration.Wcf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;

namespace CalculateService.App_Start
{
    /// <summary>
    /// Настройка маршрутизации
    /// </summary>
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Add(
                new ServiceRoute(
                    ConfigurationManager.AppSettings["CalcServiceRoute"],
                    new AutofacWebServiceHostFactory(), typeof(CalcService))
                );
        }
    }
}