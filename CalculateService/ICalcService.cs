﻿using Calculator.Common.Messages;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CalculateService
{
    /// <summary>
    /// Контракт сервера осуществляющего логику
    /// </summary>
    [ServiceContract]
    public interface ICalcService
    {
        /// <summary>
        /// Получение значение переменной
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetValueVariable?variable={variable}")]
        GetValueVariableResponse GetValueVariable(string variable);

        /// <summary>
        /// Создание новой переменной
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "CreateVariable?expression={expression}")]
        CreateVariableResponse CreateVariable(string expression);

    }

}
