﻿using CalculateService.App_Start;
using Calculator.Common;
using Calculator.Common.Messages;
using Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;

namespace CalculateService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CalcService : ICalcService
    {
        private readonly ICalculate _calculator;
        public CalcService(ICalculate calculator)
        {
            _calculator = calculator;
        }
        /// <summary>
        /// Получение значения переменной
        /// </summary>
        /// <param name="variable">имя переменной</param>
        /// <returns></returns>
        public GetValueVariableResponse GetValueVariable(string variable)
        {
            GetValueVariableResponse response = new GetValueVariableResponse();
            double? result = null;
            lock (Global.lockCalc)
            {
                result = CalculateHelper.GetVariable(variable);
            }
            if(result != null)
            {
                response.StatusCode = StatusType.OK;
                response.Value = (double) result;
            }
            else
            {
                response.StatusCode = StatusType.Error;
                response.ErrorMessage = Constants.NotValueVariable;
            }

            return response;
        }

        /// <summary>
        /// Создание новой переменной
        /// </summary>
        /// <param name="expression">выражение</param>
        /// <returns></returns>
        public CreateVariableResponse CreateVariable(string expression)
        {
            CreateVariableResponse response = new CreateVariableResponse();
            string error;
            bool isSuccess = CalculateHelper.CreateFormulaObject(_calculator, expression, out error);
            if(!isSuccess)
            {
                response.StatusCode = StatusType.Error;
                response.ErrorMessage = error;
            }
            else
            {
                response.StatusCode = StatusType.OK;
            }
            return response;

        }
    }
}