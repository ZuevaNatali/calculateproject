﻿using Calculator.Core;
using Calculator.Core.DAL;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CalculateService
{
    public static class CalculateHelper
    {
        public static readonly ILog _log = LogManager.GetLogger(typeof(CalculateHelper));

        /// <summary>
        /// Сохранение новой формулы
        /// </summary>
        /// <param name="line">входная строка</param>
        /// <param name="error">содержит ошибку если сохранение не прошло успешно</param>
        /// <returns>true если успешно сохранено</returns>
        public static bool CreateFormulaObject(ICalculate _calculator, string line, out  string error)
        {
            _log.InfoFormat("CreateFormulaObject {0}", line);
            error = String.Empty;
            string variable;
            string expression;
            Formula formula = new Formula();
            MappingContext context = new MappingContext();

            //нормализуем строку со знаком деления
            try
            {
                bool isSuccess = _calculator.TryGetPartsOfExpression(line, out variable, out expression);
                if (!isSuccess)
                {
                    error = Constants.ExpressionError;
                    return false;
                }
                var existVariable = context.Formuls.Where(f => f.VariableName == variable).ToList();
                if (existVariable.Count > 0)
                {
                    error = Constants.ExistError;
                    return false;
                }
                string rpn = _calculator.GetIntermediaryString(expression);
                formula.VariableName = variable;
                formula.InitFormula = expression;
                formula.IntermediateValue = rpn;
                context.Formuls.Add(formula);
                context.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                _log.ErrorFormat("ERROR: CreateFormulaObject {0}", ex.Message);
                _log.ErrorFormat("ERROR: CreateFormulaObject StackTrace {0}", ex.StackTrace);
                error = Constants.UnknownError;
                return false;
            }
        }

        /// <summary>
        /// Получение значения переменной из базы
        /// </summary>
        /// <param name="variableName">имя переменной</param>
        /// <returns></returns>
        public static double? GetVariable(string variableName)
        {
            _log.InfoFormat("GetVariable {0}", variableName);
            MappingContext context = new MappingContext();
            try
            {
                var formula = context.Formuls.Where(f => f.VariableName == variableName).SingleOrDefault();
                if (formula != null && formula.Result != null)
                    return formula.Result;
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("ERROR: GetVariable {0}", ex.Message);
                _log.ErrorFormat("ERROR: GetVariable StackTrace {0}", ex.StackTrace);
            }

            return null;
        }


        /// <summary>
        /// Вычисление невычисленных выражений из базы
        /// </summary>
        public static void CalculateExpressions(ICalculate calculator)
        {
            try
            {
                MappingContext context = new MappingContext();

                var list = context.Formuls.Where(f => f.Result == null).ToList();
                bool isChanges = false;
                foreach (var item in list)
                {
                    //Приводим к нужному виду
                    string normulExpression = NormalizedView(item.IntermediateValue);
                    if (normulExpression == null)
                        continue;

                    double value = calculator.CalculateIntermediaryString(normulExpression);
                    item.Result = value;
                    context.Entry(item).State = EntityState.Modified;
                    isChanges = true;
                }
                if (isChanges)
                    context.SaveChanges();
            }
            catch(Exception ex)
            {
                _log.ErrorFormat("ERROR: CalculateExpressions {0}", ex.Message);
                _log.ErrorFormat("ERROR: CalculateExpressions StackTrace {0}", ex.StackTrace);
            }
        }


        /// <summary>
        /// Нормализует выражение(заменяет переменные значениями)
        /// </summary>
        /// <param name="intermediateValue">промежуточная строка</param>
        /// <returns>преобразованное выражение или null если в выражении имеются неопределенные переменные</returns>
        private static string NormalizedView(string intermediateValue)
        {
            MappingContext context = new MappingContext();

            string resultString = intermediateValue;

            string[] elems = intermediateValue.Split(new char[] { ' '},StringSplitOptions.RemoveEmptyEntries);
            foreach (var elem in elems)
            {
                if (elem.All(c => char.IsLetter(c)))
                {
                    var variable = context.Formuls.Where(f => f.VariableName == elem).FirstOrDefault();
                    if (variable != null && variable.Result != null)
                    {
                        resultString = resultString.Replace(elem, variable.Result.ToString());
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            return resultString;
        }
    }
}