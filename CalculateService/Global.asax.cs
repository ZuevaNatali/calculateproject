﻿using Autofac;
using Autofac.Integration.Wcf;
using CalculateService.App_Start;
using Calculator.Core;
using Calculator.Core.DAL;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;


namespace CalculateService
{
    public class Global : System.Web.HttpApplication
    {
        public static readonly ILog _log = LogManager.GetLogger(typeof(Global));
        public static object lockCalc = new object();

        protected void Application_Start(object sender, EventArgs e)
        {
            InitLogger();
            _log.Info("Application Start...");
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ServiceModule());

            AutofacHostFactory.Container = builder.Build();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ContextInitializer.CheckModel();

            Thread CalculateExpressionThread = new Thread(new ThreadStart(MonitorCalcExpression));
            CalculateExpressionThread.IsBackground = true;
            CalculateExpressionThread.Start();

        }


        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            _log.FatalFormat("FATAL: {0}", ex.Message);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            _log.Info("Application_End");
        }



        private void InitLogger()
        {
            XmlConfigurator.Configure();
        }
        /// <summary>
        /// Подсчет еще невысчетанных выражений в фоновом режиме
        /// </summary>
        private static void MonitorCalcExpression()
        {
            _log.InfoFormat("MonitorCalcExpression start");
            ICalculate calculator = AutofacHostFactory.Container.Resolve<ICalculate>();
            while (true)
            {
                Thread.Sleep(10000);

                lock (lockCalc)
                {
                    CalculateHelper.CalculateExpressions(calculator);
                }
            }
        }



    }
}