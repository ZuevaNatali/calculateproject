﻿using Autofac;
using Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculateService
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RPNCalculator>().As<ICalculate>();
            builder.RegisterType<CalcService>().SingleInstance();
        }


    }
}