﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator.Core;

namespace Calculator.Tests
{
    [TestClass]
    public class RPNValidTest
    {
        [TestMethod]
        public void IsValidTest()
        {
            ICalculate calculate = new RPNCalculator();
            string expression = "A+B";
            string expression2 = "12/3";
            string expression3 = @"A/B+3";
            string expression4 = "(B+3)-1";
            string expression5 = "(B+3)*1";
            string expression6 = "0.3";
            string expression7 = "5";

            var isvalid1 = calculate.IsValidExpression(expression);
            var isvalid2 = calculate.IsValidExpression(expression2);
            var isvalid3 = calculate.IsValidExpression(expression3);
            var isvalid4 = calculate.IsValidExpression(expression4);
            var isvalid5 = calculate.IsValidExpression(expression5);
            var isvalid6 = calculate.IsValidExpression(expression6);
            var isvalid7 = calculate.IsValidExpression(expression7);

            Assert.IsTrue(isvalid1);
            Assert.IsTrue(isvalid2);
            Assert.IsTrue(isvalid3);
            Assert.IsTrue(isvalid4);
            Assert.IsTrue(isvalid5);
            Assert.IsTrue(isvalid6);
            Assert.IsTrue(isvalid7);
        }


        [TestMethod]
        public void IsNOTValidTest()
        {
            ICalculate calculate = new RPNCalculator();
            string expression = "X=A+B";
            string expression2 = "-2+12/3";
            string expression3 = @"A+B=0";
            string expression4 = ")89+0";
            string expression5 = "+789";

            var isvalid1 = calculate.IsValidExpression(expression);
            var isvalid2 = calculate.IsValidExpression(expression2);
            var isvalid3 = calculate.IsValidExpression(expression3);
            var isvalid4 = calculate.IsValidExpression(expression4);
            var isvalid5 = calculate.IsValidExpression(expression5);


            Assert.IsFalse(isvalid1);
            Assert.IsFalse(isvalid2);
            Assert.IsFalse(isvalid3);
            Assert.IsFalse(isvalid4);
            Assert.IsFalse(isvalid5);

        }


        [TestMethod]
        public void TryGetPartsOfExpressionTest()
        {
            ICalculate calculate = new RPNCalculator();
            string line = "C=A+B";
            string variable,expression;
            bool result = calculate.TryGetPartsOfExpression(line, out variable, out expression);

            Assert.IsTrue(result);
            Assert.AreEqual(variable,"C");
            Assert.AreEqual(expression, "A+B");

        }


        [TestMethod]
        public void TryGetPartsOfNegativeExpressionTest()
        {
            ICalculate calculate = new RPNCalculator();
            string line = "C=-2+B";
            string variable, expression;
            bool result = calculate.TryGetPartsOfExpression(line, out variable, out expression);

            Assert.IsTrue(result);
            Assert.AreEqual(variable, "C");
            Assert.AreEqual(expression, "0-2+B");

        }

        [TestMethod]
        public void TryGetPartsOfNegativeWithMultiplicationExpressionTest()
        {
            ICalculate calculate = new RPNCalculator();
            string line = "C=-2*15.45";
            string variable, expression;
            bool result = calculate.TryGetPartsOfExpression(line, out variable, out expression);

            Assert.IsTrue(result);
            Assert.AreEqual(variable, "C");
            Assert.AreEqual(expression, "(0-2)*15.45");

        }
    }
}
