﻿using Calculator.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Tests
{
    [TestClass]
    public class RPNCalculateTest
    {
        [TestMethod]
        public void GetRPNStringTest()
        {
            ICalculate calculate = new RPNCalculator();
            string expression = "5+10/8";
            string expression1 = "A/7+10/8";
            string expression2 = "item/7+10/8";
            string expression3 = "(A-B)/2";
   
            var rpn = calculate.GetIntermediaryString(expression);
            var rpn1 = calculate.GetIntermediaryString(expression1);
            var rpn2 = calculate.GetIntermediaryString(expression2);
            var rpn3 = calculate.GetIntermediaryString(expression3);

            Assert.AreEqual(rpn, "5 10 8 / + ");
            Assert.AreEqual(rpn1, "A 7 / 10 8 / + ");
            Assert.AreEqual(rpn2, "item 7 / 10 8 / + ");
            Assert.AreEqual(rpn3, "A B - 2 / ");
            
        }


        [TestMethod]
        public void GetRPNStringWithDoubleTest()
        {
            ICalculate calculate = new RPNCalculator();
            string expression = "5.8+10";

            var rpn = calculate.GetIntermediaryString(expression);

            Assert.AreEqual(rpn, "5.8 10 + ");

        }


        [TestMethod]
        public void CalculateRPNTest()
        {
            ICalculate calculate = new RPNCalculator();
            string rpn = "5 10 8 / + ";
            var result = calculate.CalculateIntermediaryString(rpn);
            Assert.AreEqual(result.ToString(), "6,25");

            string rpn2 = "5.8 10 + ";
            var result2 = calculate.CalculateIntermediaryString(rpn2);
            Assert.AreEqual(result2.ToString(), "15,8");

        }



        [TestMethod]
        public void Calculate2RPNTest()
        {
            ICalculate calculate = new RPNCalculator();
            string rpn = "123";
            var result = calculate.CalculateIntermediaryString(rpn);
            Assert.AreEqual(result.ToString(), "123");
        }


    }
}
