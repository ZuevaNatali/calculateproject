﻿using Calculator.Common;
using Calculator.Common.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Calculator.Tests.ServiceTests
{
    /// <summary>
    /// тесты при работающем сервисе, на тестовой чистой базе
    /// </summary>
    [TestClass]
    public class CalculateServiceTests
    {

        public static string urlGetValue = @"http://localhost:61061/calculateService/GetValueVariable?variable={0}";
        public static string urlCreateVariable = @"http://localhost:61061/calculateService/CreateVariable?expression={0}";

        [TestMethod]
        public void CreateVariableSuccessTest()
        {
            string expression = "A=67-57";
            var url = String.Format(urlCreateVariable, expression);
            CreateVariableResponse response = HttpWebHelper.GetContent<CreateVariableResponse>(url);
            Assert.AreEqual(response.StatusCode, Common.StatusType.OK);
        }

        [TestMethod]
        public void CreateVariableNotSuccessTest()
        {
            //Создание переменной
            string expression = "B=67-87";
            var url = String.Format(urlCreateVariable, expression);
            CreateVariableResponse response = HttpWebHelper.GetContent<CreateVariableResponse>(url);

            Assert.AreEqual(response.StatusCode, Common.StatusType.OK);


            //Попытка создать переменную с таким же именем
            string expression2 = "B=895";
            var url2 = String.Format(urlCreateVariable, expression2);
            CreateVariableResponse response2 = HttpWebHelper.GetContent<CreateVariableResponse>(url2);

            Assert.AreEqual(response2.StatusCode, Common.StatusType.Error);

        }

        [TestMethod]
        public void GetVariableSuccessTest()
        {
            //Создание переменной
            string expression = "C=67-87";
            var url = String.Format(urlCreateVariable, expression);

            CreateVariableResponse response = HttpWebHelper.GetContent<CreateVariableResponse>(url);
            Assert.AreEqual(response.StatusCode, Common.StatusType.OK);

            Thread.Sleep(100000);
            //Получение значения переменной
            string variable = "C";
            var url2 = String.Format(urlGetValue, variable);
            GetValueVariableResponse response2 = HttpWebHelper.GetContent<GetValueVariableResponse>(url2);

            Assert.AreEqual(response2.StatusCode, Common.StatusType.OK);
            Assert.AreEqual(response2.Value, (double)-20);

        }


        [TestMethod]
        public void GetVariableNotSuccessTest()
        {
            //Получение значения переменной
            string variable = "X";
            var url = String.Format(urlGetValue, variable);
            GetValueVariableResponse response = HttpWebHelper.GetContent<GetValueVariableResponse>(url);

            Assert.AreEqual(response.StatusCode, Common.StatusType.Error);

        }



    }
}
