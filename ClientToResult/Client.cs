﻿using Calculator.Common;
using Calculator.Common.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ClientToResult
{
    /// <summary>
    /// Класс клиента для получения результата
    /// </summary>
    public static class Client
    {
        /// <summary>
        /// Работа клиента
        /// </summary>
        public static void Start()
        {
            while (true)
            {
                Console.WriteLine("Введите имя переменной");
                string variable = Console.ReadLine();
                string result = SendResuest(variable);
                Console.WriteLine(result);
            }
        }


        /// <summary>
        /// Отправка запроса на сервер
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static string SendResuest(string variable)
        {
            string result = String.Empty;
            try
            {
                var pattern = ConfigurationManager.AppSettings["CalcService"] + "GetValueVariable?variable={0}";
                var url = String.Format(pattern, variable);
                var response = HttpWebHelper.GetContent<GetValueVariableResponse>(url);
                if (response.StatusCode != StatusType.OK)
                {
                    result = response.ErrorMessage;
                }
                else
                {
                    result = response.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                result = "Произошла ошибка";
            }
            return result;
        }


    }
}
