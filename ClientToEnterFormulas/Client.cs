﻿using Calculator.Common;
using Calculator.Common.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace ClientToEnterFormulas
{
    /// <summary>
    /// Класс клиента создающего формулы
    /// </summary>
    public static class Client
    {

        /// <summary>
        /// Работа клиента
        /// </summary>
        public static void Start()
        {
            while(true)
            {
                Console.WriteLine("Введите формулу");
                string expression = Console.ReadLine();
                string result = SendResuest(expression);
                Console.WriteLine(result);
            }
        }


        /// <summary>
        /// Отправка запроса на сервер
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static string SendResuest(string expression)
        {
            string result = "Создана";
            //expression = expression.Replace("+", "%2B");
            try
            {
                var pattern = ConfigurationManager.AppSettings["CalcService"] + "CreateVariable?expression={0}";
                var url = String.Format(pattern, HttpUtility.UrlEncode(expression));
                
                var response = HttpWebHelper.GetContent<CreateVariableResponse>(url);
                if(response.StatusCode != StatusType.OK)
                {
                    result = response.ErrorMessage;
                }
            }
            catch(Exception ex)
            {
                result = "Произошла ошибка";
            }
            return result;
        }
    }
}
